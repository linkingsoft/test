var gulp=require('gulp');
var sass=require("gulp-sass");
var sourcemaps=require("gulp-sourcemaps");

var srcPaths={
    html:"src/html/",
    scss:"src/scss/",
}

var distPaths={
    html:"dist/html/",
    css:"dist/css/",
}

gulp.task("html",function () {
    return gulp.src(srcPaths.html+"*.html")
        .pipe(gulp.dest(distPaths.html));
});

gulp.task("css",function(){
    return gulp.src((srcPaths.scss+"*.scss"))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distPaths.css));
})

// gulp.task("default",["html"],function (){});
gulp.task("default",gulp.series(['html','css'],function (){

}));